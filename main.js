var url =
  "https://api.digitalslidearchive.org/api/v1/item/5b9ef9c8e62914002e94771e/tiles/zxy/{z}/{x}/{y}?edge=crop";

// minimap

const overviewMapControl = new ol.control.OverviewMap({
  // see in overviewmap-custom.html to see the custom CSS used
  className: "ol-overviewmap ol-custom-overviewmap",
  layers: [
    new ol.layer.Tile({
      source: new ol.source.OSM({
        url: url,
      }),
    }),
  ],
  collapseLabel: "\u00BB",
  label: "\u00AB",
  collapsed: false,
});

const map = new ol.Map({
  controls: ol.control.defaults().extend([overviewMapControl]),
  layers: [
    new ol.layer.Tile({
      source: new ol.source.XYZ({
        url: url,
      }),
    }),
  ],
  target: "map",
  view: new ol.View({
    center: [40, 80],
    zoom: 2,
    constrainRotation: 16,
  }),
});

//scaleLine
const scaleLine = new ol.control.ScaleLine({bar: false, text: true, minWidth: 125});
map.addControl(scaleLine);
const modify = new ol.interaction.Modify({source: source});
map.addInteraction(modify);

let draw, snap; // global so we can remove them later
const typeSelect = document.getElementById('type');
scaleLine.setUnits("metric")

function addInteractions() {
  draw = new ol.interaction.Draw({
    source: source,
    type: typeSelect.value,
  });
  map.addInteraction(draw);
  snap = new ol.interaction.Snap({source: source});
  map.addInteraction(snap);
}

/**
 * Handle change event.
 */
typeSelect.onchange = function () {
  map.removeInteraction(draw);
  map.removeInteraction(snap);
  addInteractions();
};

addInteractions();

map.getViewport().addEventListener('contextmenu', function (e) {
  e.preventDefault();
  console.info(e)
});
